# DownMediaDownloader

[![pipeline status](https://gitlab.com/downmediaprograms/downmediadownloader/badges/master/pipeline.svg)](https://gitlab.com/shortstring96/downmediadownloader/-/commits/master)

A way to download media from many different websites.

## Supported links

Find supported links [here](https://gitlab.com/downmediaprograms/downmediadownloader/-/wikis/Supported-links)

## Building and Running

Find build instructions [here](https://gitlab.com/downmediaprograms/downmediadownloader/-/wikis/Running-DownMediaDownloader)
