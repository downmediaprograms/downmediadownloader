import ffmpeg from "fluent-ffmpeg";
import { mkdirSync, readFileSync, unlink, writeFile, writeFileSync } from "fs";
import fetch from "node-fetch";
import path from "path";
import readline from "readline";
import { configType } from "../types/Config";
import arraydl, { DownloadArray } from "./ArrayDL";
import { cleanString, fileExists } from "./Misc";

// Number of download threads
let threads = 10;

let outputDir: string = __dirname;

export async function M3U8_Download(url: string, saveDir: string, options?: configType["M3U8"], title?: string): Promise<void> {
    // Update output dir
    outputDir = saveDir;

    // Update options
    if (options) {
        threads = options.downloadThreads;
    }

    const splitUrl = url.split('/');

    if (!title) {
        console.log(`\nNow downloading: ${url}`);
        // Generate a title if none is given
        title = `${new Date().getTime()} - ${splitUrl[splitUrl.length - 1]}`;
    }

    // File name and path to save to
    const outputFile = path.join(outputDir, `${cleanString(title)}.mp4`);

    // Check if output already exists
    if (fileExists(outputFile)) {
        console.log('Output already exists');
        return;
    }

    // Make output and temp dir
    mkdirSync(outputDir, { recursive: true });
    mkdirSync(path.join(outputDir, 'temp'), { recursive: true });

    // Get playlist file
    const playlist = await (await fetch(url)).text();

    // split playlist into lines
    const playlistLines = playlist.split(/\r?\n/);

    // Add segments to an array
    const segments: string[] = [];
    for (let i = 0; i < playlistLines.length; i++) {
        const line = playlistLines[i];
        if (!line.startsWith('#') && line !== '') {
            segments.push(line);
        }
    }

    //#region This is the biggest compatibility issue
    // This is a poor way to do this, but it works for Twitter and Twitch
    if (segments[0].startsWith('http')) {
        url = '';
    } else if (segments[0].startsWith('/')) {
        // Prefix partial url with domain name
        url = `${url.split('//')[0]}//${url.split('//')[1].split('/')[0]}`;
    } else {
        // Just assume segment numbers (0.ts, etc)
        // Append segment to url
        const baseUrlSplit = url.split('/');
        baseUrlSplit.pop();
        url = baseUrlSplit.join('/');
        if (!url.endsWith('/'))
            url += '/';
    }
    //#endregion

    // Make a DownloadArray
    const downloadArray: DownloadArray = [];
    for (let i = 0; i < segments.length; i++) {
        downloadArray.push({ "URL": `${url}${segments[i]}`, "path": path.join(outputDir, 'temp', `${i}.ts`) });
    }

    // Write file with list of complete segments
    let completeSeg: number[], lastWritten: number[];
    const fileInv = setInterval(() => {
        if (completeSeg?.length === lastWritten?.length) return;
        lastWritten = [...completeSeg];
        writeFile(path.join(outputDir, 'temp', 'complete'), completeSeg.toString(), (err) => {
            if (err) console.error(err);
        });
    }, 5 * 1000);

    let resumeData: undefined | number[];

    if (fileExists(path.join(outputDir, 'temp', 'complete'))) {
        console.log("Resuming download");
        const raw = readFileSync(path.join(outputDir, 'temp', 'complete'), 'utf-8');
        const splitRaw = raw.split(',');
        resumeData = [];
        for (const int of splitRaw) {
            resumeData?.push(+int);
        }
    }

    await arraydl(downloadArray, { "threads": threads, "skip": resumeData }, (bytesDownloaded, percent, _errors, complete) => {
        completeSeg = complete;
        const toMB = (bytes: number) => { return (bytes / 1000 / 1000).toFixed(2); };
        readline.cursorTo(process.stdout, 0);
        process.stdout.write(`Downloading ${percent.toFixed(2)}% (${toMB(bytesDownloaded)}MB)`);
        readline.moveCursor(process.stdout, 0, 0);
    });

    clearInterval(fileInv);

    await combineSegments(playlist, outputFile)
        .catch(err => {
            console.error(err);
            console.warn('Transcode failed.');
        });

    return;
}

function combineSegments(playlist: string, outputFile: string): Promise<void> {
    return new Promise((resolve, reject) => {

        //#region Remap m3u8 file for ffmpeg
        const playlistSplit = playlist.split(/\r?\n/);
        const remapped: string[] = [];
        const sourceFiles: string[] = [
            path.join(outputDir, 'temp', 'manifest.m3u8'),
            path.join(outputDir, 'temp', 'complete')
        ];

        let segmentLine = 0;
        for (let i = 0; i < playlistSplit.length; i++) {
            const line = playlistSplit[i].trim();

            // Ignore non-file lines
            if (!line.startsWith('#')) {
                const segPath = path.join(outputDir, 'temp', `${segmentLine++}.ts`);
                remapped.push(segPath);
                sourceFiles.push(segPath);
            } else {
                remapped.push(line);
            }
        }

        const manifestPath = path.join(outputDir, 'temp', 'manifest.m3u8');

        writeFileSync(manifestPath, remapped.join('\n'));
        //#endregion

        // Print newline
        process.stdout.write('\n');

        const ffStream = ffmpeg(manifestPath)
            .videoCodec('copy') // Only need to copy
            .audioCodec('copy')
            .on('progress', info => {
                if (!info.percent) return; // Sometimes percent is missing
                readline.cursorTo(process.stdout, 0);
                process.stdout.write(`Transcoding ${info.percent.toFixed(2)}%`);
                readline.moveCursor(process.stdout, 0, 0);
            })
            .on('error', err => {
                console.error(err);
                reject('Transcode failed');
            })
            .on('end', () => {
                console.log('\nTranscode complete.');

                // Delete source files
                for (let i = 0; i < sourceFiles.length; i++) {
                    const file = sourceFiles[i];
                    unlink(file, () => {
                        return;
                    });
                }

                resolve();
            });

        ffStream.save(outputFile);
    });
}
