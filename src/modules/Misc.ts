import { statSync } from "fs";

// Remove illegal characters from a string
export function cleanString(string: string): string {
    // Define illegal characters
    const illegal = /[/\\?%*:|"<>]/g;

    // Replace illegal characters
    const clean = string.replace(illegal, ' ');

    // Return clean string
    return clean.trim();
}

export function fileExists(path: string): boolean {
    try {
        statSync(path);
    } catch (error) {
        return false;
    }
    return true;
}
