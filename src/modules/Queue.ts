import { readFileSync, unlinkSync, writeFileSync } from "fs";
import path from "path";
import { fileExists } from "./Misc";

// Where to store the queue file
const queuePath = path.join(__dirname, '..', 'queue.json');

type queueType = { "rows": { url: string, savePath?: string }[] };

// Delete queue file
export function deleteQueue(): void {
    try {
        unlinkSync(queuePath);
    } catch (error) {
        console.error("Failed to delete queue file");
    }
}

// Delete row in queue
export function deleteRow(row: number): void {
    const current = readQueue();

    if (!current) return;

    current.splice(row, 1);

    writeQueue(current, true);
}

// Read data from queue
export function readQueue(): undefined | queueType["rows"] {
    if (!fileExists(queuePath)) return;

    const raw = readFileSync(queuePath, { encoding: 'utf8' });
    const queue: queueType = JSON.parse(raw);

    return queue.rows;
}

// Write data to queue
export function writeQueue(item: { url: string, savePath?: string } | queueType["rows"], overwrite?: boolean): void {
    const rows: queueType["rows"] = [];

    if (!overwrite) {
        const current = readQueue();

        if (current) {
            for (let i = 0; i < current.length; i++) {
                const row = current[i];
                rows.push(row);
            }
        }
    }

    let out: queueType;

    if (!Array.isArray(item)) {
        rows.push({ url: item.url, savePath: item.savePath });

        out = {
            rows: rows
        };
    } else {
        out = {
            rows: item
        };
    }

    writeFileSync(queuePath, JSON.stringify(out));
}
