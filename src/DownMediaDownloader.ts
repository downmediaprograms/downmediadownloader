/**
 * DownMediaDownloader.ts
 * ----------------------
 * Always consider copyright before downloading
 */

import { mkdirSync, statSync, writeFileSync } from "fs";
import path from "path";
import ytpl from "ytpl";
import { question } from "./modules/Async";
import { readConfig } from "./modules/Config";
import { M3U8_Download } from "./modules/M3U8";
import { Monstercat_Download } from "./modules/Monstercat";
import { deleteQueue, deleteRow, readQueue, writeQueue } from "./modules/Queue";
import { Soundcloud_Download } from "./modules/Soundcloud";
import { Twitch_download } from "./modules/Twitch";
import { Twitter_Download } from "./modules/Twitter";
import { YouTube_Download } from "./modules/YouTube";

// Define output path, config path, and config
const outputDir = path.join(__dirname, 'output');
export const configPath = path.join(__dirname, 'config.json');
const config = readConfig();

(() => {
    try {
        statSync(path.join(__dirname, 'queue.json'));
        console.warn('Using existing queue');
    } catch (error) {
        // Error means no queue exists
    }
})();

promptUser();

// Prompt user for links
async function promptUser(): Promise<void> {
    const response = await question('Paste a supported url > ');

    if (!response) return promptUser(); // Catch empty response

    const command = response.trim().toLowerCase();

    if (command === 'done' || command === 'download' || command === 'start') {
        return sortUrls();
    }

    if (command === 'exit') {
        process.exit(0);
    }

    // Process YouTube playlists
    if (response.includes('youtube.com')) {
        // Check if item is a playlist or channel
        if (response.includes('/playlist?') || response.includes('/c/') || response.includes('/channel/')) {
            console.log('Adding playlist');

            const added = await resolvePlaylist(response)
                .catch(err => {
                    console.error(err);
                    console.warn('Failed to get playlist');
                });

            if (added) console.log(`Added ${added} videos`);

            return promptUser();
        }
    }

    // Add response to queue
    writeQueue({ url: response });

    // Loop
    promptUser();
}

async function sortUrls(): Promise<void> {
    const queue = readQueue();

    if (queue)
        for (let i = 0; i < queue.length; i++) {
            const row = queue[i];
            const url = row.url;
            let out = outputDir;

            // Set row-specific output directory, if there is one
            if (row.savePath) out = row.savePath;

            if (!url) continue;

            if (url.endsWith('.m3u8')) { // If the url leads to a manifest file
                await M3U8_Download(url, out, config.M3U8);
                deleteRow(0);
                continue;
            }

            if (url.includes("monstercat.com")) { // If the url is from Monstercat
                await Monstercat_Download(url, out);
                deleteRow(0);
                continue;
            }

            if (url.includes("soundcloud.com")) {
                await Soundcloud_Download(url, out);
                deleteRow(0);
                continue;
            }

            if (url.includes("twitter.com")) { // If the url is from Twitter
                await Twitter_Download(url, out);
                deleteRow(0);
                continue;
            }

            if (url.includes("twitch.tv/videos")) { // If the url is from twitch and is a vod
                await Twitch_download(url, out, config.Twitch, config.M3U8);
                deleteRow(0);
                continue;
            }

            if (url.includes("youtu")) { // If the url is from YouTube
                await YouTube_Download(url, out, config.YouTube);
                deleteRow(0);
                continue;
            }

            // Unknown url
            console.warn(`Unknown url: ${url}`);
            deleteRow(0);
        }

    deleteQueue();
    console.log('\nDownloads complete.\n');

    // Sometimes it can take a second for a write call to happen
    // This shouldn't be necessary, but just to be safe.
    setTimeout(() => {
        process.exit(0);
    }, 5000);
}

// Process YouTube playlists
export function resolvePlaylist(url: string): Promise<number> {
    return new Promise((resolve, reject) => {
        ytpl(url, { limit: Infinity })
            .catch(reject)
            .then((list) => {
                if (!list) return reject('List was not defined');

                const out = path.join(outputDir, `${list.title} - ${list.author.name}`);

                const titles: string[] = [];

                for (let i = 0; i < list.items.length; i++) {
                    const item = list.items[i];
                    writeQueue({ url: item.url, savePath: out });
                    titles.push(`${i + 1} ${item.title}`);
                }

                mkdirSync(out, { recursive: true });

                writeFileSync(path.join(out, 'List order.txt'), titles.join('\n'));

                resolve(list.items.length);
            });
    });
}
