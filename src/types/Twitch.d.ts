// Twitch info response
// Lots of this is unused, and probably can be removed
export type Type_TwitchInfo = {
    "error"?: string;
    "title": string;
    "description": string;
    "description_html": string;
    "broadcast_id": number;
    "broadcast_type": string;
    "status": string;
    "tag_list": string;
    "views": number;
    "url": string;
    "language": string;
    "created_at": string;
    "viewable": string;
    "viewable_at": null;
    "published_at": string;
    "delete_at": string;
    "_id": string;
    "recorded_at": string;
    "game": string;
    "length": number;
    "muted_segments"?: {
        "duration": number;
        "offset": number;
    }[];
    "preview": {
        "small": string;
        "medium": string;
        "large": string;
        "template": string;
    };
    "animated_preview_url": string;
    "thumbnails": {
        "small": ObjectConstructor[][];
        "medium": ObjectConstructor[][];
        "large": ObjectConstructor[][];
        "template": ObjectConstructor[][];
    };
    "fps": {
        "chunked": number;
    };
    "seek_previews_url": string;
    "resolutions": {
        "chunked": string;
    };
    "restriction": string;
    "channel": {
        "mature": boolean;
        "status": string;
        "broadcaster_language": string;
        "broadcaster_software": string;
        "display_name": string;
        "game": string;
        "language": string;
        "_id": number;
        "name": string;
        "created_at": string;
        "updated_at": string;
        "partner": boolean;
        "logo": string;
        "profile_banner": string;
        "profile_banner_background_color": string;
        "url": string;
        "views": number;
        "followers": number;
        "broadcaster_type": string;
        "description": string;
        "private_video": boolean;
        "privacy_options_enabled": boolean;
    };
    "increment_view_count_url": string;
};

export type Type_TwitchVideoMoments = {
    data: {
        video: {
            id: string;
            moments: {
                edges: {
                    cursor: string;
                    node: {
                        id: string;
                        durationMilliseconds: number;
                        positionMilliseconds: number;
                        type: string;
                        description: string;
                        thumbnailURL: string;
                        details: {
                            game: {
                                id: string;
                                displayName: string;
                                botArtURL: string;
                            };
                        };
                        video: {
                            id: string;
                            lengthSeconds: number;
                        };
                    };
                }[];
            };
        };
    };
    extensions: {
        durationMilliseconds: number;
        operationName: string;
        requestID: string;
    };
}[];

export type Type_TwitchComments = {
    "comments": ({
        "_id": string;
        "created_at": string;
        "updated_at": string;
        "channel_id": string;
        "content_type": string;
        "content_id": string;
        "content_offset_seconds": number;
        "commenter": {
            "display_name": string;
            "_id": string;
            "name": string;
            "type": string;
            "bio": string | null;
            "created_at": string;
            "updated_at": string;
            "logo": string;
        };
        "source": string;
        "state": string;
        "message": {
            "body": string;
            "bits_spent"?: number;
            fragments: [
                {
                    "text": string;
                    "emoticon"?: {
                        "emoticon_id": string;
                        "emoticon_set_id": string;
                    };
                }
            ];
            "is_action": boolean;
            user_badges?: [
                {
                    "_id"?: string;
                    "version"?: number;
                }
            ];
            "user_color": string;
            "user_notice_params": {
                "msg-id"?: string;
                "msg-param-gift-months"?: string;
                "msg-param-months"?: string;
                "msg-param-origin-id"?: string;
                "msg-param-recipient-display-name"?: string;
                "msg-param-recipient-id"?: string;
                "msg-param-recipient-user-name"?: string;
                "msg-param-sender-count"?: string;
                "msg-param-sub-plan"?: string;
                "msg-param-sub-plan-name"?: string;
            };
        };
    })[];
    "_next": string;
};

export type Type_TwitchCheermotes = {
    "actions": {
        "prefix": string;
        "scales": string[];
        "tiers": {
            "min_bits": number;
            "id": string;
            "color": string;
            "images": {
                "dark": {
                    "animated": {
                        "1": string;
                        "1.5": string;
                        "2": string;
                        "3": string;
                        "4": string;
                    };
                    "static": {
                        "1": string;
                        "1.5": string;
                        "2": string;
                        "3": string;
                        "4": string;
                    };
                };
                "light": {
                    "animated": {
                        "1": string;
                        "1.5": string;
                        "2": string;
                        "3": string;
                        "4": string;
                    };
                    "static": {
                        "1": string;
                        "1.5": string;
                        "2": string;
                        "3": string;
                        "4": string;
                    };
                };
            };
            "can_cheer": boolean;
        }[];
        // I'm just assuming that these can be undefined
        "backgrounds": [
            "light" | undefined,
            "dark" | undefined
        ],
        "states": [
            "static" | undefined,
            "animated" | undefined
        ],
        "type": string,
        "priority": number
    }[];
}

// BTTV global
export type t_bttvGlobal = {
    "id": string;
    "code": string;
    "imageType": string;
    "userId": string;
}[];

// BTTV channel
export type t_bttvChannel = {
    "id"?: string;
    "bots"?: string[];
    "channelEmotes"?: {
        "id": string;
        "code": string;
        "imageType": string;
        "userId": string;
    }[];
    "sharedEmotes"?: {
        "id": string;
        "code": string;
        "imageType": string;
        "user": {
            "id": string;
            "name": string;
            "displayName": string;
            "providerId": string;
        };
    }[];
};

// FFZ
export type t_ffz = {
    "id": number;
    "user": {
        "id": number;
        "name": string;
        "displayName": string;
    };
    "code": string;
    "images": {
        "1x": string;
        "2x": string | null;
        "4x": string | null;
    };
    "imageType": string;
}[];
